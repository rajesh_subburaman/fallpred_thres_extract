function [y] = hampel_sw(x,k,t0)
% t0 = 0;
n = length(x);
y = x;
ind = [];
L = 1.4826;

for ii=1:length(x)
%  keyboard
% disp('-------------------------------------------')
% disp(ii)
ind1 = ii-k;
if(ind1<=0)
    ind1 = ii;
end
ind2 = ii+k;
if(ind2>length(x))
    ind2 = length(x);
end
    x0 = median(x(ind1:ind2));
%     disp(x0);
    S0 = L*median(abs(x(ind1:ind2)-x0));
%     disp('Median 2')
%     disp(median(abs(x(ind1:ind2)-x0)));
%     disp('S0:')
%     disp(S0)
%     disp('x(i)-x0')
%     disp(abs(x(ii)-x0))
    if(abs(x(ii)-x0)>t0*S0)        
        y(ii) = x0;
        ind = [ind,ii];
    end
end

end

