function [m] = medfilt1w(s, w)
if nargin==1
    w=5;
end
s = s(:)';
w2 = floor(w/2);
w = 2*w2+1;

n = length(s);
m = zeros(w,n+w-1);
s0 = s(1); 
sl = s(n);
keyboard
for ii=0:(w-1)
    m(ii+1,:) = [s0*ones(1,ii) s sl*ones(1,w-ii-1)];
end

m = median(m);
m = m(w2+1:w2+n);
end

