function [y] = hampelw(x,k,t0)
% t0 = 0;
n = length(x);
y = x;
ind = [];
L = 1.4826;

for ii=k+1:(n-k)
%     keyboard
    x0 = median(x((ii-k):(ii+k)));
    S0 = L*median(abs(x((ii-k):(ii+k))-x0));
    if(abs(x(ii)-x0)>t0*S0)
        y(ii) = x0;
        ind = [ind,ii];
    end
end

end

