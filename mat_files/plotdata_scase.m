function [ ] = plotdata_scase(data)
%% Defining variables and extracting data
dat_len = length(data.position0);

% time conversion (nano secs -> min)
%  time = time/(1*(10^18));
% time = data.Time;
% time = time -(ones(dat_len,1)*time(1,1));

% CoG data
CG_pos = [data.position0 data.position1 data.position2]; 
CG_vel = [data.velocity0 data.velocity1 data.velocity2]; 
CG_acc = [data.acceleration0 data.acceleration1 data.acceleration2];

% IMU data
IMU_orient = [data.orientation0 data.orientation1 data.orientation2];
IMU_angvel = [data.angular_vel0 data.angular_vel1 data.angular_vel2];
IMU_linacc = [data.linear_acc0 data.linear_acc1 data.linear_acc2];

% ZMP data
ZMP = [data.zmp0 data.zmp1 data.zmp2];

% Feet data
Lf_force = [data.Lfeet_force0 data.Lfeet_force1 abs(data.Lfeet_force2)];
Rf_force = [data.Rfeet_force0 data.Rfeet_force1 abs(data.Rfeet_force2)];% % time conversion (nano secs -> min)
% time = data.Ftime;
time = data.Time;
time = time -(ones(dat_len,1)*time(1,1));
Lf_conArea = data.Lfeet_conArea0;
Rf_conArea = data.Rfeet_conArea0;
rob_conArea = data.rob_conArea0;

Pforce = [data.PushForce0 data.PushForce1 data.PushForce2];
Pforce_mag = (Pforce(:,1).^2)+(Pforce(:,2).^2)+(Pforce(:,3).^2);
Pforce_mag = sqrt(Pforce_mag);

OPF_max = [data.OptF_max0];
% OPF_min = [data.OptF_min0 data.OptF_min1];

% tmp = find(Pforce_mag>0);
% if(length(tmp)>5)
% Pfrate = round(length(tmp)/3);
% else
%     Pfrate = 2;
% end
% tmp1 = hampel_sw(Pforce_mag(tmp),Pfrate,1);
% [val,ind] = max(Pforce_mag(tmp));
% tmp2 = find(Pforce_mag>(val-50));
% 
% if(length(tmp2)>1)
%     Impulse = sum(Pforce_mag(tmp2)*0.001)/length(tmp2);
% else
%     Impulse = Pforce_mag(tmp2)*0.001;
% end
%     tsteps = length(tmp2);
% Pforce_mag(tmp) = tmp1;
%Max Ind
% MaxInd = [data.MaxInd0 data.MaxInd1 data.MaxInd2 data.MaxInd3 data.MaxInd4 data.MaxInd5 data.MaxInd6 data.MaxInd7 ...
%           data.MaxInd8 data.MaxInd9 data.MaxInd10 data.MaxInd11];
% MinInd = [data.MinInd0 data.MinInd1 data.MinInd2 data.MinInd3 data.MinInd4 data.MinInd5 data.MinInd6 data.MinInd7 ...
%           data.MinInd8 data.MinInd9 data.MinInd10 data.MinInd11];      

% Fall Indicator
% FallIndicator = data.FallIndicator;

%% Filtering data (using hampel)

frate1 = 51;
Pfrate = 11;
frate1_ac = 61;
frate1_zmp = 101;
t0 = 1;
for ii=1:3
%     CG_pos(:,ii) = hampel_sw(CG_pos(:,ii),1,t0);
    CG_vel(:,ii) = hampel_sw(CG_vel(:,ii),frate1,t0);
    CG_acc(:,ii) = hampel_sw(CG_acc(:,ii),frate1,t0);
    
%     IMU_orient(:,ii) = hampel_sw(IMU_orient(:,ii),1,t0);
    IMU_angvel(:,ii) = hampel_sw(IMU_angvel(:,ii),frate1_ac,t0);
    IMU_linacc(:,ii) = hampel_sw(IMU_linacc(:,ii),frate1,t0);
    
    ZMP(:,ii) = hampel_sw(ZMP(:,ii),frate1_zmp,t0);
    
    Lf_force(:,ii) = hampel_sw(Lf_force(:,ii),frate1_zmp,t0);
    Rf_force(:,ii) = hampel_sw(Rf_force(:,ii),frate1_zmp,t0); 
    
%     Pforce(:,ii) = hampelw(Pforce(:,ii),Pfrate,t0);
    
end

% for jj=1:2
%     OPF_max(:,jj) = hampel_sw(OPF_max(:,jj),frate1_ac,t0);
%     OPF_min(:,jj) = hampel_sw(OPF_min(:,jj),frate1_ac,t0);
% end
OPF_max = hampel_sw(OPF_max,5,t0);
Lf_conArea = hampel_sw(Lf_conArea,frate1_ac,t0);
Lf_conArea = Lf_conArea';
Rf_conArea = hampel_sw(Rf_conArea,frate1_ac,t0);
Rf_conArea = Rf_conArea';
rob_conArea = hampel_sw(rob_conArea,frate1_ac,t0);
rob_conArea = rob_conArea';

frate = 41;
frate2_zmp = 81;
for ii=1:3
%     CG_pos(:,ii) = medfilt1_sw(CG_pos(:,ii),1);
    CG_vel(:,ii) = medfilt1_sw(CG_vel(:,ii),frate1);
    CG_acc(:,ii) = medfilt1_sw(CG_acc(:,ii),frate1);
    
%     IMU_orient(:,ii) = medfilt1_sw(IMU_orient(:,ii),1);
    IMU_angvel(:,ii) = medfilt1_sw(IMU_angvel(:,ii),frate1_ac);
    IMU_linacc(:,ii) = medfilt1_sw(IMU_linacc(:,ii),frate1);
    
    ZMP(:,ii) = medfilt1_sw(ZMP(:,ii),frate2_zmp);
    
%     Lf_force(:,ii) = medfilt1_sw(Lf_force(:,ii),frate1);
%     Rf_force(:,ii) = medfilt1_sw(Rf_force(:,ii),frate1); 
    
%     Pforce(:,ii) = medfilt1w(Pforce(:,ii),Pfrate);    
end

% for jj=1:2
%     OPF_max(:,jj) = medfilt1_sw(OPF_max(:,jj),31);
%     OPF_min(:,jj) = medfilt1_sw(OPF_min(:,jj),31);
% end
OPF_max = medfilt1_sw(OPF_max,5);

% Lf_conArea = medfilt1_sw(Lf_conArea,frate1_ac);
% Lf_conArea = Lf_conArea';
% Rf_conArea = medfilt1_sw(Rf_conArea,frate1_ac);
% Rf_conArea = Rf_conArea';
% rob_conArea = medfilt1_sw(rob_conArea,frate1_ac);
% rob_conArea = rob_conArea';

%% Plotting data

%COG data plot
H1 = figure(1);set(H1,'Name','CoG Data')
subplot(3,1,1);
plot(time,CG_pos(:,1),'r','linewidth',1.5);
hold on
plot(time,CG_pos(:,2),'g','linewidth',1.5);
plot(time,CG_pos(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('dist [m]','Fontsize',12)
legend('CG_x','CG_y','CG_z');
title('CG Position','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(3,1,2);
plot(time,CG_vel(:,1),'r','linewidth',1.5);
hold on
plot(time,CG_vel(:,2),'g','linewidth',1.5);
plot(time,CG_vel(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('vel [m/s]','Fontsize',12)
legend('CG_{vx}','CG_{vy}','CG_{vz}');
title('CG Velocity','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(3,1,3);
plot(time,CG_acc(:,1),'r','linewidth',1.5);
hold on
plot(time,CG_acc(:,2),'g','linewidth',1.5);
plot(time,CG_acc(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('acc [m2/s]','Fontsize',12)
xlabel('time[s]','Fontsize',12)
legend('CG_{ax}','CG_{ay}','CG_{az}');
title('CG Acceleration','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

% IMU data plot
H2 = figure(2);set(H2,'Name','IMU Data')
subplot(3,1,1);
plot(time,IMU_orient(:,1),'r','linewidth',1.5);
hold on
plot(time,IMU_orient(:,2),'g','linewidth',1.5);
plot(time,IMU_orient(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('orient [rad]','Fontsize',12)
legend('IMU_x','IMU_y','IMU_z');
title('Body Orientation','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(3,1,2);
plot(time,IMU_angvel(:,1),'r','linewidth',1.5);
hold on
plot(time,IMU_angvel(:,2),'g','linewidth',1.5);
plot(time,IMU_angvel(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('ang vel [rad/s]','Fontsize',12)
legend('IMU_{avx}','IMU_{avy}','IMU_{avz}');
title('Angular velocity','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(3,1,3);
plot(time,IMU_linacc(:,1),'r','linewidth',1.5);
hold on
plot(time,IMU_linacc(:,2),'g','linewidth',1.5);
plot(time,IMU_linacc(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('lin.acc [m2/s]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
legend('IMU_{lx}','IMU_{ly}','IMU_{lz}');
title('Linear acceleration','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

% ZMP data plot
figure(3)
plot(time,ZMP(:,1),'r','linewidth',1.5);
hold on
plot(time,ZMP(:,2),'g','linewidth',1.5);
plot(time,ZMP(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('distance [m]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
legend('ZMP_x','ZMP_y','ZMP_z');
title('ZMP','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

% Feet force and contact area data plot
figure(4)
subplot(2,2,1);
plot(time,Lf_force(:,1),'r','linewidth',1.5);
hold on
plot(time,Lf_force(:,2),'g','linewidth',1.5);
plot(time,Lf_force(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('Force [N]','Fontsize',12)
legend('F_x','F_y','F_z');
title('Left feet force','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(2,2,2);
plot(time,Rf_force(:,1),'r','linewidth',1.5);
hold on
plot(time,Rf_force(:,2),'g','linewidth',1.5);
plot(time,Rf_force(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('Force [N]','Fontsize',12)
legend('F_x','F_y','F_z');
title('Right feet force','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(2,2,3);
plot(time,Lf_conArea,'r','linewidth',1.5);
xlim([0 time(end)])
ylabel('area [m2]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
%legend('CGxAcc','CGyAcc','CGzAcc');
title('Contact Area(left)','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

subplot(2,2,4);
plot(time,Rf_conArea,'r','linewidth',1.5);
xlim([0 time(end)])
ylabel('area [m2]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
%legend('CGxAcc','CGyAcc','CGzAcc');
title('Contact Area(right)','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

% Robot Contact Area
figure(5)
plot(time,rob_conArea,'r','linewidth',1.5);
xlim([0 time(end)])
ylabel('area [m2]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
%legend('CGxAcc','CGyAcc','CGzAcc');
title('Contact Area(Total)','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

% Pushing force
figure(6)
plot(time,Pforce_mag,'r','linewidth',1.5);
% hold on
% plot(time,Pforce(:,2),'g','linewidth',1.5);
% plot(time,Pforce(:,3),'b','linewidth',1.5);
xlim([0 time(end)])
ylabel('Force [N]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
legend('PFmag');
title('Pushing force','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

figure(7)
plot(time,OPF_max,'r','linewidth',1.5);
% hold on
% plot(time,OPF_max(:,2),'g','linewidth',1.5);
xlim([0 time(end)])
ylabel('pixel vel. [m/s]','Fontsize',12)
xlabel('time [s]','Fontsize',12)
title('Optical Flow - Velocity magnitude','Fontsize',12)
ax = gca;
ax.FontSize = 12;
ax.FontWeight = 'bold';

% subplot(2,1,2)
% plot(time,OPF_min(:,1),'r','linewidth',1.5);
% hold on
% % plot(time,OPF_min(:,2),'g','linewidth',1.5);
% xlim([0 time(end)])
% ylabel('pixel_vel [m/s]','Fontsize',12)
% xlabel('time [s]','Fontsize',12)
% legend('OPFmin_x','OPFmin_y');
% title('Optical Flow - Min','Fontsize',12)

%Fall Indicator
% figure(8)
% plot(time,FallIndicator,'r','linewidth',1.5);
% xlim([0 time(end)])
% ylabel('IndValue','Fontsize',12)
% xlabel('time [s]','Fontsize',12)
% legend('IndSum');
% title('Fall Indicator','Fontsize',12)
end

