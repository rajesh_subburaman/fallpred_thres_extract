function [maxV,minV,findex,findex_min,Impulse,tsteps] = get_MaxMin_MFI(fpath,fname)

% feature variable order
Fvar_ord = {'CoG_pos','CoG_vel','CoG_acc','IMU_ort','IMU_angvel','IMU_linacc','ZMP','Lforce',...
            'Rforce','Lmean_conA','Rmean_conA','Lf_Con','Rf_Con','Lforce_grad','Rforce_grad',...
            'Op_flow','std_Opf_grad','rob_conA','std_rob_conA'};
disp('Feature variables considered:');
disp(Fvar_ord);

% No. of feature variables
nVar = 37;

% Initializing the variables
flength = cellfun(@length,fname);
maxV = -1000*ones(nVar,1);
minV = 1000*ones(nVar,1);

tmaxV = zeros(nVar,1);
tminV = zeros(nVar,1);
findex = ones(nVar,1);
Impulse = zeros(flength,1);
tsteps = zeros(flength,1);

Lf_x = zeros(flength,1001);
Lf_y = zeros(flength,1001);
Lf_z = zeros(flength,1001);

Rf_x = zeros(flength,1001);
Rf_y = zeros(flength,1001);
Rf_z = zeros(flength,1001);

Lf_g = zeros(flength,1001);
Rf_g = zeros(flength,1001);
L_con = zeros(flength,1001);
R_con = zeros(flength,1001);

OPF_gvec = zeros(flength,1001);

for ll=1:flength
disp(ll);

% Loading the rtp files 
filename = strcat(fpath,fname{1}{ll});
n = char(filename);
[time, data] = rtpload(n);

% Defining variables and extracting data
dat_len = length(data.position0);

% time conversion (nano secs -> min)
time = time/(1*(10^9));
time = time -(ones(dat_len,1)*time(1,1));

% CoG data
CG_pos = [data.position0 data.position1 data.position2]; 
CG_vel = [data.velocity0 data.velocity1 data.velocity2]; 
CG_acc = [data.acceleration0 data.acceleration1 data.acceleration2];

% IMU data
IMU_orient = [data.orientation0 data.orientation1 data.orientation2];
IMU_angvel = [data.angular_vel0 data.angular_vel1 data.angular_vel2];
IMU_linacc = [data.linear_acc0 data.linear_acc1 data.linear_acc2];

% ZMP data
ZMP = [data.zmp0 data.zmp1 data.zmp2];

% Feet data
Lf_force = [data.Lfeet_force0 data.Lfeet_force1 abs(data.Lfeet_force2)];
Rf_force = [data.Rfeet_force0 data.Rfeet_force1 abs(data.Rfeet_force2)];
Lf_conArea = data.Lfeet_conArea0;
Rf_conArea = data.Rfeet_conArea0;
rob_conArea = data.rob_conArea0;

% Pushing Impulse
Pforce = [data.PushForce0 data.PushForce1 data.PushForce2];
Pforce_mag = (Pforce(:,1).^2)+(Pforce(:,2).^2)+(Pforce(:,3).^2);
Pforce_mag = sqrt(Pforce_mag);

% Optical flow
OPF_max = [data.OptF_max0];

% Computing the applied impulse
tmp = find(Pforce_mag>0);
[val,ind] = max(Pforce_mag(tmp));
tmp2 = find(Pforce_mag>(val-50));
if(length(tmp2)>1)
    Impulse(ll) = sum(Pforce_mag(tmp2)*0.001)/length(tmp2);
else
    Impulse(ll) = Pforce_mag(tmp2)*0.001;
end
    tsteps(ll) = length(tmp2);
    
% Filtering data (using hampel)
frate1 = 51;
Pfrate = 11;
frate1_ac = 61;
frate1_zmp = 101;
t0 = 1;

for ii=1:3
%     CG_pos(:,ii) = hampel_sw(CG_pos(:,ii),frate1,t0);
    CG_vel(:,ii) = hampel_sw(CG_vel(:,ii),frate1,t0);
    CG_acc(:,ii) = hampel_sw(CG_acc(:,ii),frate1_ac,t0);
    
%     IMU_orient(:,ii) = hampel_sw(IMU_orient(:,ii),frate1,t0);
    IMU_angvel(:,ii) = hampel_sw(IMU_angvel(:,ii),frate1,t0);
    IMU_linacc(:,ii) = hampel_sw(IMU_linacc(:,ii),frate1_ac,t0);
    
    ZMP(:,ii) = hampel_sw(ZMP(:,ii),frate1_zmp,t0);
    
    Lf_force(:,ii) = hampel_sw(Lf_force(:,ii),Pfrate,t0);
    Rf_force(:,ii) = hampel_sw(Rf_force(:,ii),Pfrate,t0); 
    
%    Pforce(:,ii) = hampel_sw(Pforce(:,ii),Pfrate,t0);    
end

OPF_max = hampel_sw(OPF_max,21,t0);

rob_conArea = hampel_sw(rob_conArea,frate1_ac,t0);

% Filtering data (using median)
frate = 41;
frate2_zmp = 81;

for ii=1:3
%   CG_pos(:,ii) = medfilt1_sw(CG_pos(:,ii),frate);
    CG_vel(:,ii) = medfilt1_sw(CG_vel(:,ii),frate);
    CG_acc(:,ii) = medfilt1_sw(CG_acc(:,ii),frate);
    
%     IMU_orient(:,ii) = medfilt1_sw(IMU_orient(:,ii),frate);
    IMU_angvel(:,ii) = medfilt1_sw(IMU_angvel(:,ii),frate);
    IMU_linacc(:,ii) = medfilt1_sw(IMU_linacc(:,ii),frate);
    
    ZMP(:,ii) = medfilt1_sw(ZMP(:,ii),frate2_zmp);
    
    Lf_force(:,ii) = medfilt1_sw(Lf_force(:,ii),frate);
    Rf_force(:,ii) = medfilt1_sw(Rf_force(:,ii),frate); 
    
%     Pforce(:,ii) = medfilt1w(Pforce(:,ii),Pfrate);    
end

OPF_max = medfilt1_sw(OPF_max,11);
Lf_force_g = gradient(Lf_force(:,3));
Rf_force_g = gradient(Rf_force(:,3));


Opf_g = gradient(OPF_max);

% Sets the time window for non-fall data observation
[v ind1] = max(Pforce_mag);
ind1 = ind1 + 0;
ind2 = ind1 + 1000;
if(ind2>length(Pforce_mag))
    ind2 = length(Pforce_mag);
end

% Left foot point and surface contact frequency
indlf_pCon = find(Lf_conArea(ind1:ind2)<=0);
indlf_sCon = find(Lf_conArea(ind1:ind2)>0);
fqlf_pCon = length(indlf_pCon)/length(Lf_conArea(ind1:ind2));
fqlf_sCon = length(indlf_sCon)/length(Lf_conArea(ind1:ind2));

% Right foot point and surface contact frequency
indrf_pCon = find(Rf_conArea(ind1:ind2)<=0);
indrf_sCon = find(Rf_conArea(ind1:ind2)>0);
fqrf_pCon = length(indrf_pCon)/length(Rf_conArea(ind1:ind2));
fqrf_sCon = length(indrf_sCon)/length(Rf_conArea(ind1:ind2));


% Auxillary variables to avoid NaN values
if((ind2-ind1+1)<1001)
    efill = 1001-(ind2-ind1+1);
else
    efill = 0;
end

Lf_x(ll,1:(ind2-ind1+1)) = Lf_force(ind1:ind2,1);
Lf_y(ll,1:(ind2-ind1+1)) = Lf_force(ind1:ind2,2);
Lf_z(ll,1:(ind2-ind1+1)) = Lf_force(ind1:ind2,3);
Rf_x(ll,1:(ind2-ind1+1)) = Rf_force(ind1:ind2,1);
Rf_y(ll,1:(ind2-ind1+1)) = Rf_force(ind1:ind2,2);
Rf_z(ll,1:(ind2-ind1+1)) = Rf_force(ind1:ind2,3);

Lf_g(ll,1:(ind2-ind1+1)) = Lf_force_g(ind1:ind2);
Rf_g(ll,1:(ind2-ind1+1)) = Rf_force_g(ind1:ind2);

L_con(ll,1:(ind2-ind1+1)) = Lf_conArea(ind1:ind2);
R_con(ll,1:(ind2-ind1+1)) = Rf_conArea(ind1:ind2);
OPF_gvec(ll,1:(ind2-ind1+1)) = Opf_g(ind1:ind2); 


if(efill>0)
    Lf_x(ll,(ind2-ind1+1):1001) = Lf_x(ll,ind2-ind1+1); 
    Lf_y(ll,(ind2-ind1+1):1001) = Lf_y(ll,ind2-ind1+1); 
    Lf_z(ll,(ind2-ind1+1):1001) = Lf_z(ll,ind2-ind1+1); 
    Rf_x(ll,(ind2-ind1+1):1001) = Rf_x(ll,ind2-ind1+1); 
    Rf_y(ll,(ind2-ind1+1):1001) = Rf_y(ll,ind2-ind1+1); 
    Rf_z(ll,(ind2-ind1+1):1001) = Rf_z(ll,ind2-ind1+1); 
    
    Lf_g(ll,(ind2-ind1+1):1001) = Lf_g(ll,ind2-ind1+1);
    Rf_g(ll,(ind2-ind1+1):1001) = Rf_g(ll,ind2-ind1+1);
    
    L_con(ll,(ind2-ind1+1):1001) = L_con(ll,ind2-ind1+1);
    R_con(ll,(ind2-ind1+1):1001) = R_con(ll,ind2-ind1+1); 
    OPF_gvec(ll,(ind2-ind1+1):1001) = OPF_gvec(ll,ind2-ind1+1); 
end

% Deteriming the thresholds from the selected time window for different
% feature variables.
for jj=1:nVar
    if(jj<=3)
    tmax(jj) = max(CG_pos(ind1:ind2,jj));
    tmin(jj) = min(CG_pos(ind1:ind2,jj));
    elseif((jj>3)&&(jj<=6))
        tmax(jj) = max(CG_vel(ind1:ind2,jj-3));
        tmin(jj) = min(CG_vel(ind1:ind2,jj-3));
    elseif((jj>6)&&(jj<=9))
        tmax(jj) = max(CG_acc(ind1:ind2,jj-6));
        tmin(jj) = min(CG_acc(ind1:ind2,jj-6));
    elseif((jj>9)&&(jj<=12))
        tmax(jj) = max(IMU_orient(ind1:ind2,jj-9));
        tmin(jj) = min(IMU_orient(ind1:ind2,jj-9));
    elseif((jj>12)&&(jj<=15))
        tmax(jj) = max(IMU_angvel(ind1:ind2,jj-12));
        tmin(jj) = min(IMU_angvel(ind1:ind2,jj-12));
    elseif((jj>15)&&(jj<=18))
        tmax(jj) = max(IMU_linacc(ind1:ind2,jj-15));
        tmin(jj) = min(IMU_linacc(ind1:ind2,jj-15));
    elseif((jj>18)&&(jj<=21))        
        tmax(jj) = max(ZMP(ind1:ind2,jj-18));        
        tmin(jj) = min(ZMP(ind1:ind2,jj-18));
    elseif((jj>21)&&(jj<=24))
        tmax(jj) = max(Lf_force(ind1:ind2,jj-21));
        tmin(jj) = min(Lf_force(ind1:ind2,jj-21));
    elseif((jj>24)&&(jj<=27))
        tmax(jj) = max(Rf_force(ind1:ind2,jj-24));
        tmin(jj) = min(Rf_force(ind1:ind2,jj-24));
    elseif(jj==28)
        tmax(jj) = mean(Lf_conArea(ind1:ind2));
        tmin(jj) = mean(Lf_conArea(ind1:ind2));
    elseif(jj==29)
        tmax(jj) = mean(Rf_conArea(ind1:ind2));
        tmin(jj) = mean(Rf_conArea(ind1:ind2));    
    elseif(jj==30)
        tmax(jj) = fqlf_pCon;
        tmin(jj) = fqlf_sCon;
    elseif(jj==31)
        tmax(jj) = fqrf_pCon;
        tmin(jj) = fqrf_sCon; 
    elseif(jj==32)
        tmax(jj) = max(Lf_force_g(ind1:ind2));
        tmin(jj) = min(Lf_force_g(ind1:ind2));
    elseif(jj==33)
        tmax(jj) = max(Rf_force_g(ind1:ind2));
        tmin(jj) = min(Rf_force_g(ind1:ind2));
    elseif(jj==34)
        tmax(jj) = max(OPF_max(ind1:ind2));
        tmin(jj) = min(OPF_max(ind1:ind2));
    elseif(jj==35)
        tmax(jj) = std(Opf_g(ind1:ind2));
        tmin(jj) = std(Opf_g(ind1:ind2));
    elseif(jj==36)    
        tmax(jj) = mean(rob_conArea(ind1:ind2));
        tmin(jj) = mean(rob_conArea(ind1:ind2));
    else
        tmax(jj) = std(rob_conArea(ind1:ind2));
        tmin(jj) = std(rob_conArea(ind1:ind2));
    end
end

for kk=1:nVar
    if(tmax(kk)>maxV(kk))
        maxV(kk) = tmax(kk);
        findex(kk) = ll; % stores the file index which yields the max value
    end
    if(tmin(kk)<minV(kk))
        minV(kk) = tmin(kk); 
        findex_min(kk) = ll; % stores the file index which yields the min value
    end
end

end

