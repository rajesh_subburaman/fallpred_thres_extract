%%%%% Fall Data Analysis %%%%%%
close all
clear all
clc

% location of the mat files
addpath(genpath('C:\Users\RAJESH\Documents\IIT_PhD\Fall_Detection\rel_mat_files'))
%% Data Visualization
%{
  To visualize the filtered data collected for different features
  during a fall or non-fall experiment. The following code displays the
  evolution of pushing force, ZMP, CoG, IMU, Foot pressure sensor, and
  Optical flow values on individual plots.

  Functions:
  1) rtpload('filepath')
     Loads the data from an rtp file generated during the experiment.
     
     Parameters: 
        filepath - the path location of a .rtp file whose data needs to be visualized.    
     Returns:
        time    -   log time vector of double type.
        data    -   a structure array which includes the raw data collected
                    for each feature.
  
  2) plotdata_scase('data')
     Reads the data from the given structure as input, segregates and filters the data according to the features, 
     and finally plots them on separate plots. 

     Parameters: 
        data    -   a structure array which includes the raw data collected
                    for each feature by exerting disturbance to the robot.
     Returns:
        Individual plots showing the evolution of different feature values
        before and after the application of disturbance.
%}

% [time, data] = rtpload('/home/rajesh/catkin_ws/check_data.rtp');
% [time, data] = rtpload('C:\Users\RAJESH\Documents\IIT_PhD\Fall_Detection\Simulation Data\Scenario4_101116\falldata_750_03.rtp');
% plotdata_scase(data);

%% Threshold Extraction

%{
  Thresholds are extracted for each feature variables for four major scenarios using the data collected during their 
  respective non-fall experiments. The details about the non-fall experiments and the experimental setup are given in 
  the paper.

  Functions:
  1) getData('scn_loc','scn')
     Given a specific scenario as an input, the function returns the non-fall and fall case rtp files and their respective
     file locations.

     Parameters:
        scn_loc     -   location of the data collected for four different scenarios.
        scn         -   a positive integer 1 to 4 representing the four major scenarios.
     Returns:
        NFname  -   the rtp file names of non-fall cases to be considered.
        Fname   -   the rtp file names of fall cases to be considered.

  2) get_Thres('scn_loc','NFname')
     Given the rtp file location of fall cases and the file names to be considered, the function determines the 
     maximum and minimum value of each feature variable from their respective collected data during the experiments.

     Parameters:
        scn_loc -   location of the data collected for four different scenarios.
        NFname  -   the rtp file names of non-fall cases to be considered.
     Returns:
        maxV_nf, minV_nf    -   maximum and minimum value of the feature variables.
        findex, findex_min  -   the indices of the thresholds which basically denote the experiment to which they belong.
        Impulse             -   the impulse subjected to the robot during the experiments.
        tsteps              -   number of time steps considered from the instant of the disturbance for the extraction 
                                of thresholds.
%}
total_scn = 4;

thres_fileNames={'max_Thres_101116S1.txt','min_Thres_101116S1.txt',...
                 'max_Thres_101116S2.txt','min_Thres_101116S2.txt',...
                 'max_Thres_101116S3.txt','min_Thres_101116S3.txt',...
                 'max_Thres_101116S4.txt','min_Thres_101116S4.txt'};

for k=1:total_scn
    scn = k;
    text = strcat('Generating thresholds for scenario: ',num2str(scn));
    disp(text);
    close all;
    
    data_loc = {'C:\Users\RAJESH\Documents\IIT_PhD\Fall_Detection\Simulation Data\'};
    scn_fldr = {'Scenario1_101116\','Scenario2_101116\','Scenario3_101116\','Scenario4_101116\'};
    scn_loc = strcat(data_loc,scn_fldr{scn});
    [NFname,Fname] = getData(scn_loc,scn);
    [maxV_nf,minV_nf,findex,findex_min,Impulse,tsteps] = get_Thres(scn_loc,NFname);     

%% Threshold Visualization

%{
  The thresholds extracted for each feature variables are visualized and verified by comparing them to their respective 
  maximum and minimum values of each fall cases. The lower and upper thresholds are represented in the form of light blue 
  and magenta lines respectively, while the maximum and minimum values of each fall cases are denoted by blue and red 
  stars. If either of the stars are outside the threshold lines, then the extracted thresholds are considered to be good 
  enough in classifying the falls. 

  Functions:
  1) plotThre_scn('scn_loc','Fname','maxV_nf','minV_nf')
     Given the rtp file location of fall cases, the rtp file names, and the extracted thresholds, the function returns 
     individual plots for each feature variable demonstrating visually how successful the extracted thresholds are in 
     classifying the fall cases. 

     Parameters:
        scn_loc             -   location of the data collected for four different scenarios.
        Fname               -   the rtp file names of fall cases to be considered.
        maxV_nf, minV_nf    -   maximum and minimum value of the feature variables.
     Returns:
        Individual plots for each feature variable demonstrating visually its ability to classify the falls.
%}
    prompt = 'Do you want to visualize the thresholds? Y/N : ';
    str = input(prompt,'s');
    if(str=='Y')
        plotThres_scn(scn_loc,Fname,maxV_nf,minV_nf);
        str = 'N';
    end

%% Generate Threshold Text File

%{
  The thresholds determined above for each scenario are saved as text files. The maximum and minimum threshold values are 
  saved in separate files. These files are used later for predicting the fall over of humanoids online.

  Functions:
  1) write_file('thres_fileLoc','sel_thresFnames','maxV_nf','minV_nf')
     Given the desired threshold file names, file location, maximum and minimum thresholds, 
     the function generates the threshold text files.

     Parameters:
        thres_fileLoc       -   location where the threshold text files needs to be stored.
        sel_thresFnames     -   user defined file names for the threshold files given in a 
                                cell array.
        maxV_nf, minV_nf    -   maximum and minimum value of the feature variables.

%}
    sInd = (scn-1)*2 + 1;
    eInd = scn*2;
    thres_fileLoc = 'C:\Users\RAJESH\Documents\IIT_PhD\Fall_Detection\Thresholds\';
    sel_thresFnames = thres_fileNames(sInd:eInd);    
    write_file(thres_fileLoc,sel_thresFnames,maxV_nf,minV_nf);
    pause
end