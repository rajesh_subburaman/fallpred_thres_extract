function [m] = medfilt1_sw(s, w)
if nargin==1
    w=5;
end

n = length(s);
m = s;

for ii=1:length(s)

ind1 = ii-w;
if(ind1<=0)
    ind1 = ii;
end
ind2 = ii+w;
if(ind2>length(s))
    ind2 = length(s);
end
    m(ii) = median(s(ind1:ind2));
end

