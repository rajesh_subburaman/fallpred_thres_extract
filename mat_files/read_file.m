close all
clear all
clc

%% Loading the prediction values for different sensors

% fpath = {'/home/rajesh/catkin_ws/devel/lib/fall_detection/'};
fpath = {'/home/rajesh/PhD_Work/Fall_Detection/mat_files/IROS_2017_data/'};
fname = {'Fall_PredDet121116_CoG.txt','Fall_PredDet121116_IMU.txt','Fall_PredDet121116_ZMP.txt',...
         'Fall_PredDet121116_VIS.txt','Fall_PredDet121116_FPS.txt','Fall_PredDet261116_CAP.txt',...
         'Fall_PredDet261116_CFP.txt'};
% fname = {'Fall_PredDet171116_UVT_CoG.txt','Fall_PredDet171116_UVT_IMU.txt','Fall_PredDet171116_UVT_ZMP.txt',...
%          'Fall_PredDet171116_UVT_VIS.txt','Fall_PredDet171116_UVT_FPS.txt','Fall_PredDet171116_UVT_CAP.txt',...
%          'Fall_PredDet261116_CFP_UVT.txt'};  
% fname = {'Fall_PredDet211116_MVT_CoG.txt','Fall_PredDet211116_MVT_IMU.txt','Fall_PredDet211116_MVT_ZMP.txt',...
%          'Fall_PredDet211116_MVT_VIS.txt','Fall_PredDet211116_MVT_FPS.txt','Fall_PredDet261116_MVT_CAP.txt',...
%          'Fall_PredDet261116_MVT_CFP.txt'};  
% fname = {'Fall_PredDet231116_MVR_CoG.txt','Fall_PredDet231116_MVR_IMU.txt','Fall_PredDet231116_MVR_ZMP.txt',...
%          'Fall_PredDet231116_MVR_VIS.txt','Fall_PredDet231116_MVR_FPS.txt','Fall_PredDet261116_MVR_CAP.txt',...
%          'Fall_PredDet261116_MVR_CFP.txt'};     
     
Title = {'CoG','IMU','ZMP','OPF','FC','CAP','CFP','5FS'};     

% weights for different performance indices
wt = [0.9,0.1,0.0,0.0];
     
flength = length(fname);     

for ll=1:flength     
filename = strcat(fpath,fname(ll));
n = char(filename);
disp(ll)
fileID = fopen(n);
formatSpec = '%f';
sizeA = [7 inf];
Fpred_dat = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
Fpred_dat = Fpred_dat';

sen = ll;
switch sen
    case 1
        Fpred_CoG = Fpred_dat;
    case 2
        Fpred_IMU = Fpred_dat;
    case 3
        Fpred_ZMP = Fpred_dat;
    case 4
        Fpred_VIS = Fpred_dat;
    case 5
        Fpred_FPS = Fpred_dat;
    case 6
        Fpred_CAP = Fpred_dat;
    case 7
        Fpred_CFP = Fpred_dat;
    case 8
        Fpred_5FS = Fpred_dat;
end


% Mean time
if(ll==1)
FN = zeros(1,flength);
FP = zeros(1,flength);
FPr = zeros(1,flength);
LFPr = zeros(1,flength);
LFPr_t = zeros(1,flength);
FPr_t = zeros(1,flength);

Fp_time = zeros(size(Fpred_dat,1),flength);
Ff_time = zeros(size(Fpred_dat,1),flength);
Fxang = zeros(size(Fpred_dat,1),flength);
Fyang = zeros(size(Fpred_dat,1),flength);
end

temp_ind = find(Fpred_dat(:,5)>0);
tsel = Fpred_dat(temp_ind,6)-Fpred_dat(temp_ind,5);
m_cg = mean(tsel);

Fp_time(:,ll) = Fpred_dat(:,5);
Ff_time(:,ll) = Fpred_dat(:,6);
Fxang(:,ll) = Fpred_dat(:,3);
Fyang(:,ll) = Fpred_dat(:,4);

FPr_t(ll) = sum(tsel);

FP(ll) = length(temp_ind);
FP(ll) = length(Fpred_dat)-FP(ll);

LFPr(ll) = length(find(abs(Fxang(:,ll))>0.25))+length(find(abs(Fyang(:,ll))>0.25));
if(LFPr(ll)>=(size(Fpred_dat,1)))
    LFPr(ll) = (size(Fpred_dat,1))-FP(ll);
end
FPr(ll) = length(Fpred_dat)-FP(ll)-LFPr(ll);
LFPr_ind = (find(abs(Fxang(:,ll))>0.25));
LFPr_ind = [LFPr_ind;(find(abs(Fyang(:,ll))>0.25))];
LFPr_t(ll) = sum(Ff_time(LFPr_ind,ll)-Fp_time(LFPr_ind,ll));
if(FPr(ll)>0)
FPr_t(ll) = (FPr_t(ll)-LFPr_t(ll))/FPr(ll);
else
    FPr_t(ll) = 0;
end
LFPr_t(ll) = LFPr_t(ll)/LFPr(ll);

%% Plots
if(ll==1)
Labels = {'FPr', 'LFPr', 'FP', 'FN'};
H1 = figure(1);set(H1,'Name','Performance Comparison')
[q,r] = quorem(sym(flength),sym(3));
rw = eval(q);
if(eval(r)~=0)
    rw = rw + 1;
end
end
subplot(rw,3,ll);
x = [FPr(ll),LFPr(ll),FP(ll),FN(ll)];
bar(x);
set(gca, 'XTick', 1:4, 'XTickLabel', Labels);
grid on
if(ll==4)
ylabel('No.Scenarios','Fontsize',12)
end
ylim([0 size(Fpred_dat,1)])
n1 = char(Title(ll));
title(n1,'Fontsize',14)

end

% Percentage calculation
FPr_p = (FPr/size(Fpred_dat,1))*100;
LFPr_p = (LFPr/size(Fpred_dat,1))*100;
FP_p = (FP/size(Fpred_dat,1))*100;

% Optimal weight for combining the sensors based on indiv performances
FPr_val = (FPr)*wt(1);
LFPr_val = (LFPr)*wt(2);
FP_val = (FP)*wt(3);
FN_val = (FN)*wt(4);

wt_mat = [FPr_val(1:5)' LFPr_val(1:5)' FP_val(1:5)' FN_val(1:5)'];
wt_vec = zeros(1,5);
wt_vec = (sum(wt_mat'))/(norm(sum(wt_mat'),1));  


% Time comparison
Labels = {'CoG', 'IMU', 'ZMP', 'OPF', 'FC','CAP','CFP','5FS'};
H3 = figure(3);set(H3,'Name','Time Comparison')
subplot(1,2,1);
x = [];
for ii=1:flength
    x = [x FPr_t(ii)];
end
bar(x);
set(gca, 'XTick', 1:flength, 'XTickLabel', Labels);
grid on
ylabel('Time [s]','Fontsize',14)
ylim([0 max(FPr_t)])
title('FPr avg-time','Fontsize',14)

subplot(1,2,2);
x = [];
for ii=1:flength
    x = [x LFPr_t(ii)];
end
bar(x);
set(gca, 'XTick', 1:flength, 'XTickLabel', Labels);
grid on
ylabel('Time [s]','Fontsize',14)
ylim([0 max(FPr_t)])
title('LFPr avg-time','Fontsize',14)

H4 = figure(4);set(H4,'Name','Overall Score Comparison')
PF_score = zeros(flength,1);
for ii=1:flength
    PF_score(ii) = (FPr(ii)*FPr_t(ii)*0.6) + (LFPr(ii)*LFPr_t(ii)*0.4);
end
[PF_desc,ind_desc] = sort(PF_score);
Labels_desc = Labels(ind_desc);
% for jj=1:flength
%     Labels_desc(jj) = Labels(ind_desc);
% end
barh(PF_desc)
set(gca, 'YTick', 1:flength, 'YTickLabel', Labels_desc);
grid on
xlabel('Performance Score','Fontsize',14)
title('Overall Score Comparison','Fontsize',14)

%% Precision and Recall rate

Fscore = zeros(1,flength);
for ii=1:length(FPr)
    tp = FPr(ii) + LFPr(ii);
fp = FP(ii);
Prec = tp/(tp+fp);
Rec = 1;
Fscore(ii) = (2*(Prec*Rec))/(Prec + Rec); 
end

H5 = figure(5);set(H5,'Name','F Score Comparison')

[F_desc,ind_desc] = sort(Fscore);
Labels_desc = Labels(ind_desc);
% for jj=1:flength
%     Labels_desc(jj) = Labels(ind_desc);
% end
barh(F_desc)
set(gca, 'YTick', 1:flength, 'YTickLabel', Labels_desc);
grid on
xlabel('F Score','Fontsize',14)
title('F Score Comparison','Fontsize',14)