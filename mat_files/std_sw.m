function [std_dev] = std_sw(inp_v)
bwidth = 50;
[q,r]=quorem(sym(size(inp_v,1)),sym(bwidth));
nw = eval(q);

rem = eval(r);
if(nw==0)
    disp('vector size need to be reversed !!!');   
    if(size(inp_v,1)==1)        
    inp_v = inp_v';   
    end
    if(nw<bwidth)
    bwidth=length(inp_v);
    end
    [q,r]=quorem(sym(size(inp_v,1)),sym(bwidth));
    nw = eval(q);
    rem = eval(r);
    disp(nw);
end
std_vec = zeros(nw,1);
for ii=1:nw    
    spos = (ii-1)*bwidth+1;
    v = inp_v(spos:spos+bwidth-1,1);
    std_vec(ii) = std(v);
end
if(rem~=0)
    std_vec = [std_vec;std(inp_v((nw*bwidth)+1:end,1))];
end
std_dev = max(std_vec);
