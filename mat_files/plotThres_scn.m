function [ ] = plotMaxMin_scn_MFI(fpath,fname,maxV_nf,minV_nf)

% Disturbance heights considered for training data
bname = [0.0, 0.3, 0.5, 0.7, 0.9, 1.1];

% Variable initialization
use_findex = 0;
maxV_f = [];
minV_f = [];

fsize = cellfun(@length,fname);
for jj=1:fsize
    disp('Fall data')
    disp(jj)
filename = strcat(fpath,fname{1}{jj});
n = char(filename);
[time, data] = rtpload(n);

% Defining variables and extracting data
dat_len = length(data.position0);

% time conversion (nano secs -> min)
time = time/(1*(10^9));
time = time -(ones(dat_len,1)*time(1,1));

% CoG data
CG_pos = [data.position0 data.position1 data.position2]; 
CG_vel = [data.velocity0 data.velocity1 data.velocity2]; 
CG_acc = [data.acceleration0 data.acceleration1 data.acceleration2];

% IMU data
IMU_orient = [data.orientation0 data.orientation1 data.orientation2];
IMU_angvel = [data.angular_vel0 data.angular_vel1 data.angular_vel2];
IMU_linacc = [data.linear_acc0 data.linear_acc1 data.linear_acc2];

% ZMP data
ZMP = [data.zmp0 data.zmp1 data.zmp2];

% Feet data
Lf_force = [data.Lfeet_force0 data.Lfeet_force1 abs(data.Lfeet_force2)];
Rf_force = [data.Rfeet_force0 data.Rfeet_force1 abs(data.Rfeet_force2)];
Lf_conArea = data.Lfeet_conArea0;
Rf_conArea = data.Rfeet_conArea0;
Pforce = [data.PushForce0 data.PushForce1 data.PushForce2];
Pforce_mag = (Pforce(:,1).^2)+(Pforce(:,2).^2)+(Pforce(:,3).^2);
Pforce_mag = sqrt(Pforce_mag);

OPF_max = [data.OptF_max0];
rob_conArea = data.rob_conArea0;

% Filtering data (using hampel)
frate1 = 51;
Pfrate = 11;
frate1_ac = 61;
frate1_zmp = 101;

t0 = 1;
for ii=1:3
%     CG_pos(:,ii) = hampel_sw(CG_pos(:,ii),frate1,t0);
    CG_vel(:,ii) = hampel_sw(CG_vel(:,ii),frate1,t0);
    CG_acc(:,ii) = hampel_sw(CG_acc(:,ii),frate1_ac,t0);
    
%     IMU_orient(:,ii) = hampel_sw(IMU_orient(:,ii),frate1,t0);
    IMU_angvel(:,ii) = hampel_sw(IMU_angvel(:,ii),frate1,t0);
    IMU_linacc(:,ii) = hampel_sw(IMU_linacc(:,ii),frate1_ac,t0);
    
    ZMP(:,ii) = hampel_sw(ZMP(:,ii),frate1_zmp,t0);
    
    Lf_force(:,ii) = hampel_sw(Lf_force(:,ii),Pfrate,t0);
    Rf_force(:,ii) = hampel_sw(Rf_force(:,ii),Pfrate,t0); 
    
%     Pforce(:,ii) = hampelw(Pforce(:,ii),Pfrate);
    
end

OPF_max = hampel_sw(OPF_max,5,t0);
rob_conArea = hampel_sw(rob_conArea,frate1,t0);
rob_conArea = rob_conArea';

% Filtering data (using median)
frate = 41;
frate2_zmp = 81;

for ii=1:3
%     CG_pos(:,ii) = medfilt1_sw(CG_pos(:,ii),frate);
    CG_vel(:,ii) = medfilt1_sw(CG_vel(:,ii),frate);
    CG_acc(:,ii) = medfilt1_sw(CG_acc(:,ii),frate);
    
%     IMU_orient(:,ii) = medfilt1_sw(IMU_orient(:,ii),frate);
    IMU_angvel(:,ii) = medfilt1_sw(IMU_angvel(:,ii),frate);
    IMU_linacc(:,ii) = medfilt1_sw(IMU_linacc(:,ii),frate);
    
    ZMP(:,ii) = medfilt1_sw(ZMP(:,ii),frate2_zmp);
    
    Lf_force(:,ii) = medfilt1_sw(Lf_force(:,ii),frate);
    Rf_force(:,ii) = medfilt1_sw(Rf_force(:,ii),frate); 
    
%     Pforce(:,ii) = medfilt1w(Pforce(:,ii),Pfrate);    
end


OPF_max = medfilt1_sw(OPF_max,5);


Lf_force_g = gradient(Lf_force(:,3));
Rf_force_g = gradient(Rf_force(:,3));
Opf_g = gradient(OPF_max);

% Extract max values
[v ind1] = max(Pforce_mag);
% ind1 = ind1 + 100;
f_ind1 = find((1.5-abs(IMU_orient(:,1)))<0.075,1); 
f_ind2 = find((1.5-abs(IMU_orient(:,2)))<0.075,1);

if(isempty(f_ind1)&&isempty(f_ind2))
    disp('Enters the leaning situation')
    [v1 f_ind1] = max(abs(IMU_orient(:,1)));
    [v2 f_ind2] = max(abs(IMU_orient(:,2)));
    use_findex = 1;
    if(v2>v1)
        f_ind1 = 100000;
    else
        f_ind2 = 100000;
    end
end
if(~(isempty(f_ind1))&&~(isempty(f_ind2)))
    if(f_ind2<f_ind1)
        f_ind = f_ind2;
    else
        f_ind = f_ind1;
    end
elseif (~(isempty(f_ind1)))
    f_ind = f_ind1;
elseif (~(isempty(f_ind2)))
    f_ind = f_ind2;
else
    f_ind = [];
end

f_period = f_ind-ind1;
if(use_findex==1)
    ind2 = round(ind1 + (f_period));
    use_findex = 0;
else
    ind2 = round(ind1 + (0.6*f_period));
end

% Left foot point and surface contact frequency
indlf_pCon = find(Lf_conArea(ind1:ind2)<=0.2*0.0512);
indlf_sCon = find(Lf_conArea(ind1:ind2)>0.9*0.0512);
fqlf_pCon = length(indlf_pCon)/length(Lf_conArea(ind1:ind2));
fqlf_sCon = length(indlf_sCon)/length(Lf_conArea(ind1:ind2));

% Right foot point and surface contact frequency
indrf_pCon = find(Rf_conArea(ind1:ind2)<=0.2*0.0512);
indrf_sCon = find(Rf_conArea(ind1:ind2)>0.9*0.0512);
fqrf_pCon = length(indrf_pCon)/length(Rf_conArea(ind1:ind2));
fqrf_sCon = length(indrf_sCon)/length(Rf_conArea(ind1:ind2));

%% Threshold visualizing plots

% CoG feature -- Position
figure(1)
H1 = figure(1);set(H1,'Name','CoG Threshold')
maxV = max(CG_pos(ind1:ind2,1));
minV = min(CG_pos(ind1:ind2,1));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,1)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(1)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(1)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
ylabel('dist [m]','Fontsize',14)
title('CG_x','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';
 
maxV = max(CG_pos(ind1:ind2,2));
minV = min(CG_pos(ind1:ind2,2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,2)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
     plot(bname,maxV_nf(2)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(2)*ones(size(bname)),'m', 'LineWidth', 4)
end
 axis([0 1.1 -inf inf]);
%  ylabel('dist [m]','Fontsize',14)
title('CG_y','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(CG_pos(ind1:ind2,3));
minV = min(CG_pos(ind1:ind2,3));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,3)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(3)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(3)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
% ylabel('dist [m]','Fontsize',14)
title('CG_z','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

% CoG feature -- Velocity
maxV = max(CG_vel(ind1:ind2,1));
minV = min(CG_vel(ind1:ind2,1));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,4)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(4)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(4)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
ylabel('vel [m/s]','Fontsize',14)
xlabel('height [m]', 'Fontsize', 14)
title('CG_{vx}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(CG_vel(ind1:ind2,2));
minV = min(CG_vel(ind1:ind2,2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,5)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(5)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(5)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
% ylabel('vel [m/s]','Fontsize',14)
xlabel('height [m]', 'Fontsize', 14)
title('CG_{vy}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(CG_vel(ind1:ind2,3));
minV = min(CG_vel(ind1:ind2,3));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,6)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(6)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(6)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
% ylabel('vel [m/s]','Fontsize',14)
xlabel('height [m]', 'Fontsize', 14)
title('CG_{vz}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

%% IMU feature - orientation
figure(3)
H3 = figure(3);set(H3,'Name','IMU Threshold')
maxV = max(IMU_orient(ind1:ind2,1));
minV = min(IMU_orient(ind1:ind2,1));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,1)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(10)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(10)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
ylabel('angle [rad]','Fontsize',14)
title('IMU_{ox}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';
 
maxV = max(IMU_orient(ind1:ind2,2));
minV = min(IMU_orient(ind1:ind2,2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,2)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(11)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(11)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
%  ylabel('angle [rad]','Fontsize',14)
title('IMU_{oy}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(IMU_orient(ind1:ind2,3));
minV = min(IMU_orient(ind1:ind2,3));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,3)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(12)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,maxV_nf(12)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
% ylabel('angle [rad]','Fontsize',14)
title('IMU_{oz}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

% IMU feature - angular velocity
maxV = max(IMU_angvel(ind1:ind2,1));
minV = min(IMU_angvel(ind1:ind2,1));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,4)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(13)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(13)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
ylabel('angVel [rad/s]','Fontsize',14)
xlabel('height [m]','Fontsize',14)
title('IMU_{avx}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(IMU_angvel(ind1:ind2,2));
minV = min(IMU_angvel(ind1:ind2,2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,5)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(14)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(14)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
% ylabel('angVel [rad/s]','Fontsize',14)
xlabel('height [m]','Fontsize',14)
title('IMU_{avy}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(IMU_angvel(ind1:ind2,3));
minV = min(IMU_angvel(ind1:ind2,3));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,3,6)
plot(bname(jj),maxV,'b*','linewidth',1.5);
hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
if(jj==1)
    plot(bname,maxV_nf(15)*ones(size(bname)),'c', 'LineWidth', 4)
    plot(bname,minV_nf(15)*ones(size(bname)),'m', 'LineWidth', 4)
end
axis([0 1.1 -inf inf]);
% ylabel('angVel [rad/s]','Fontsize',14)
xlabel('height [m]','Fontsize',14)
title('IMU_{avz}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

%% ZMP feature -- position
H5 = figure(5);
set(H5,'Name','ZMP Threshold')

maxV = max(ZMP(ind1:ind2,1));
minV = min(ZMP(ind1:ind2,1));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(1,2,1)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(19)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(19)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
  ylabel('dist [m]','Fontsize',14)
  xlabel('height [m]','Fontsize',14)
title('ZMP_x','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(ZMP(ind1:ind2,2));
minV = min(ZMP(ind1:ind2,2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(1,2,2)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(20)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(20)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
%  ylabel('dist [m]','Fontsize',14)
 xlabel('height [m]','Fontsize',14)
title('ZMP_y','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

%% FPS feature -- force
H6 = figure(6);
set(H6,'Name','Foot contact Threshold')
maxV = max(Lf_force(ind1:ind2,3));
minV = min(Lf_force(ind1:ind2,3));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,2,1)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(24)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(24)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
 ylabel('force [N]','Fontsize',14)
title('LF_z','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(Rf_force(ind1:ind2,3));
minV = min(Rf_force(ind1:ind2,3));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,2,2)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(27)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(27)*ones(size(bname)),'m', 'LineWidth', 4)
 end
  axis([0 1.1 -inf inf]);
%   ylabel('force [N]','Fontsize',14)
title('RF_z','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

% FPS feature -- force gradient
maxV = max(Lf_force_g(ind1:ind2));
minV = min(Lf_force_g(ind1:ind2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,2,3)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(32)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(32)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
 ylabel('\nabla(force) [N]','Fontsize',14)
 xlabel('height [m]','Fontsize',14)
title('\nabla(LF_z)','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

maxV = max(Rf_force_g(ind1:ind2));
minV = min(Rf_force_g(ind1:ind2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(2,2,4)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(33)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(33)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
%   ylabel('grad [N]','Fontsize',14)
  xlabel('height [m]','Fontsize',14)
title('\nabla(RF_z)','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

%% Uncomment this and change the subplot size to plot point contact frequency
% if(ind2-ind1 >= 49)
% maxV = mean(Lf_conArea(ind1:ind1+49));
% minV = mean(Lf_conArea(ind1:ind1+49));
% else
% maxV = mean(Lf_conArea(ind1:ind2));
% minV = mean(Lf_conArea(ind1:ind2)); 
% end
% maxV_f = [maxV_f maxV];
% minV_f = [minV_f minV];
% subplot(3,2,5)
% plot(bname(jj),maxV,'b*','linewidth',1.5);
%  hold on
%  plot(bname(jj),minV,'r*','linewidth',1.5);
%  if(jj==1)
%      plot(bname,maxV_nf(28)*ones(size(bname)),'c', 'LineWidth', 4)
%      plot(bname,minV_nf(28)*ones(size(bname)),'m', 'LineWidth', 4)
%  end
%  axis([0 1.1 -inf inf]);
%   ylabel('area [m2]','Fontsize',14)
% title('LFca','Fontsize',14)
% if(ind2-ind1>=49)
% maxV = mean(Rf_conArea(ind1:ind1+49));
% minV = mean(Rf_conArea(ind1:ind1+49));
% else
% maxV = mean(Rf_conArea(ind1:ind2));
% minV = mean(Rf_conArea(ind1:ind2));
% end
% maxV_f = [maxV_f maxV];
% minV_f = [minV_f minV];
% subplot(3,2,6)
% plot(bname(jj),maxV,'b*','linewidth',1.5);
%  hold on
%  plot(bname(jj),minV,'r*','linewidth',1.5);
%  if(jj==1)
%      plot(bname,maxV_nf(29)*ones(size(bname)),'c', 'LineWidth', 4)
%      plot(bname,minV_nf(29)*ones(size(bname)),'m', 'LineWidth', 4)
%  end
%  axis([0 1.1 -inf inf]);
%  ylabel('area [m2]','Fontsize',14)
% title('RFca','Fontsize',14)


% subplot(4,2,7)
% plot(bname(jj),fqlf_pCon,'b*','linewidth',1.5);
%  hold on
%  if(jj==6)
%      plot(bname,maxV_nf(30)*ones(size(bname)),'c', 'LineWidth', 4)
%  end
%  ylabel('freq','Fontsize',12)
%  xlabel('dist height [m]','Fontsize',12)
% title('LFpCon','Fontsize',8)
% 
% subplot(4,2,8)
% plot(bname(jj),fqrf_pCon,'b*','linewidth',1.5);
%  hold on
%  if(jj==6)
%      plot(bname,maxV_nf(31)*ones(size(bname)),'c','LineWidth', 4)
%  end
%  ylabel('freq','Fontsize',12)
%  xlabel('dist height [m]','Fontsize',12)
% title('RFpCon','Fontsize',8)


%% OPF feature -- optical flow
H7 = figure(7);
set(H7,'Name','OPF Threshold')

maxV = max(OPF_max(ind1:ind2,1));
minV = min(OPF_max(ind1:ind2,1));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(1,2,1)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(34)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(34)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
  ylabel('pixel vel. [m/s]','Fontsize',14)
  xlabel('height [m]','Fontsize',14)
title('OPF_{vel}','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

% OPF feature -- optical flow gradient

maxV = std_sw(Opf_g(ind1:ind2));
minV = std_sw(Opf_g(ind1:ind2));
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
subplot(1,2,2)
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(35)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(35)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
 ylabel('\sigma(pixel vel.) [m/s]','Fontsize',14)
 xlabel('height [m]','Fontsize',14)
title('\sigma(OPF_{vel})','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

%% FPS feature -- contact area
H8 = figure(8);
set(H8,'Name','Robot Contact Area')
subplot(2,1,1)
if(ind2-ind1>=99)
    maxV = mean(rob_conArea(ind1:ind1+99));
    minV = mean(rob_conArea(ind1:ind1+99));
else
    maxV = mean(rob_conArea(ind1:ind2));
    minV = mean(rob_conArea(ind1:ind2));
end
maxV_f = [maxV_f maxV];
minV_f = [minV_f minV];
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
plot(bname(jj),minV,'r*','linewidth',1.5);
 if(jj==1)
     plot(bname,maxV_nf(36)*ones(size(bname)),'c', 'LineWidth', 4)
     plot(bname,minV_nf(36)*ones(size(bname)),'m', 'LineWidth', 4)
 end
 axis([0 1.1 -inf inf]);
  ylabel('Area [m^2]','Fontsize',14)
title('Contact Area','Fontsize',14)
ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';

% FPS feature -- standard deviation of contact area
subplot(2,1,2)
if(ind2-ind1>=99)
    maxV = std_sw(rob_conArea(ind1:ind1+99)');
else
    maxV = std_sw(rob_conArea(ind1:ind2)');
end
maxV_f = [maxV_f maxV];
minV_f = [minV_f 0];
plot(bname(jj),maxV,'b*','linewidth',1.5);
 hold on
 if(jj==1)
     plot(bname,maxV_nf(37)*ones(size(bname)),'c', 'LineWidth', 4)    
 end
 axis([0 1.1 -inf inf]);
  ylabel('\sigma(Area) [m^2]','Fontsize',14)
  xlabel('height [m]','Fontsize',14)
  title('\sigma(Contact Area)','Fontsize',14)
  ax = gca;
ax.FontSize = 10;
ax.FontWeight = 'bold';
%% Uncomment this part and change the subplot size to plot the frequency of point contact
% subplot(3,1,3)
% if(ind2-ind1>=99)
%     maxV = length(find(rob_conArea(ind1:ind1+99)<=0.5*0.16))/(length(rob_conArea(ind1:ind1+99)));
%     minV = length(find(rob_conArea(ind1:ind1+99)<=0.5*0.16))/(length(rob_conArea(ind1:ind1+99)));   
% else
%     maxV = length(find(rob_conArea(ind1:ind2)<=0.5*0.16))/(length(rob_conArea(ind1:ind2)));
%     minV = length(find(rob_conArea(ind1:ind2)<=0.5*0.16))/(length(rob_conArea(ind1:ind2)));
% end
% maxV_f = [maxV_f maxV];
% minV_f = [minV_f minV];
% plot(bname(jj),maxV,'b*','linewidth',1.5);
%  hold on
%  if(jj==1)
%      plot(bname,maxV_nf(38)*ones(size(bname)),'c', 'LineWidth', 4)     
%  end
%  axis([0 1.1 -inf inf]);
%   ylabel('Fq-PCon','Fontsize',14)
end


end

