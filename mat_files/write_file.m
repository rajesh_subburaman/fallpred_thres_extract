function [] = write_file(thres_fileLoc,thres_fileName,max_val,min_val)

% fpath = 'C:\Users\RAJESH\Documents\IIT_PhD\Fall_Detection\Thresholds\';
fpath = thres_fileLoc;
for ii=1:length(thres_fileName)
       filename = strcat(fpath,thres_fileName(ii));
       n = char(filename);
       fileID = fopen(n,'w');
        if (fileID < 0)
            error('Unable to open file %s', filename);
        end
   if(ii==1)
       val=max_val;
   else
       val=min_val;
   end
   for jj=1:length(val)       
       if(jj<=3)
           if(jj==1)
           fprintf(fileID,'%s\n','CGPos');
           end
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj>3)&&(jj<=6))
           if(jj==4)
           fprintf(fileID,'%s\n','CGVel');
           end
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj>6)&&(jj<=9))
           if(jj==7)
           fprintf(fileID,'%s\n','CGAcc');
           end
           fprintf(fileID,'%0.4f\n',val(jj)); 
       elseif((jj>9)&&(jj<=12))
           if(jj==10)
           fprintf(fileID,'%s\n','IMUorient');
           end
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj>12)&&(jj<=15))
           if(jj==13)
           fprintf(fileID,'%s\n','IMUangvel');
           end
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj>15)&&(jj<=18))
           if(jj==16)
           fprintf(fileID,'%s\n','IMUlinacc');
           end
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj>18)&&(jj<=20))
           if(jj==19)
           fprintf(fileID,'%s\n','ZMP');
           end
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==24))
           fprintf(fileID,'%s\n','LfeetForce');
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==27))
           fprintf(fileID,'%s\n','RfeetForce');
           fprintf(fileID,'%0.4f\n',val(jj));  
%        elseif((jj==28))
%            fprintf(fileID,'%s\n','LfeetArea');
%            fprintf(fileID,'%0.4f\n',val(jj));    
%        elseif((jj==29))
%            fprintf(fileID,'%s\n','RfeetArea');
%            fprintf(fileID,'%0.4f\n',val(jj));    
%        elseif((jj==30))
%            fprintf(fileID,'%s\n','Lfqcon');
%            fprintf(fileID,'%0.4f\n',val(jj));  
%        elseif((jj==31))
%            fprintf(fileID,'%s\n','Rfqcon');
%            fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==32))
           fprintf(fileID,'%s\n','LftFgrad');
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==33))
           fprintf(fileID,'%s\n','RftFgrad');
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==34))
           fprintf(fileID,'%s\n','OpFvel');
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==35))
           fprintf(fileID,'%s\n','OpFgrad');
           fprintf(fileID,'%0.4f\n',val(jj));
       elseif((jj==36))
           fprintf(fileID,'%s\n','conArea');
           fprintf(fileID,'%0.4f\n',val(jj));    
       elseif((jj==37))
           fprintf(fileID,'%s\n','std_conArea');
           fprintf(fileID,'%0.4f\n',val(jj));     
       end             
   end
   fclose(fileID); 
end

end

