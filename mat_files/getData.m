function [fname, ffname] = GetData(scn_loc,scn)
switch scn       
    
    case 1 % (Backward disturbance)
        filename = strcat(scn_loc,'scn1NF_file.txt');
        n = char(filename);
        fileID = fopen(n);
        fname = textscan(fileID,'%s');
        fclose(fileID);
        
        filename = strcat(scn_loc,'scn1F_file.txt');
        n = char(filename);
        fileID = fopen(n);
        ffname = textscan(fileID,'%s');
        fclose(fileID);
        
    case 2    % (Leftside disturbance)
        filename = strcat(scn_loc,'scn2NF_file.txt');
        n = char(filename);
        fileID = fopen(n);
        fname = textscan(fileID,'%s');
        fclose(fileID);
        
        filename = strcat(scn_loc,'scn2F_file.txt');
        n = char(filename);
        fileID = fopen(n);
        ffname = textscan(fileID,'%s');
        fclose(fileID);
        
      case 3   % (Forward disturbance) 
        filename = strcat(scn_loc,'scn3NF_file.txt');
        n = char(filename);
        fileID = fopen(n);
        fname = textscan(fileID,'%s');
        fclose(fileID);
        
        filename = strcat(scn_loc,'scn3F_file.txt');
        n = char(filename);
        fileID = fopen(n);
        ffname = textscan(fileID,'%s');
        fclose(fileID);
        
       case 4   % (Rightside disturbance) 
        filename = strcat(scn_loc,'scn4NF_file.txt');
        n = char(filename);
        fileID = fopen(n);
        fname = textscan(fileID,'%s');
        fclose(fileID);
        
        filename = strcat(scn_loc,'scn4F_file.txt');
        n = char(filename);
        fileID = fopen(n);
        ffname = textscan(fileID,'%s');
        fclose(fileID);  

end
