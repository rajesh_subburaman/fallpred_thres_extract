******************************************************************
Threshold for Fall Prediction
=============================
	This package generates the thresholds required for predicting the fall over of humanoids. The thresholds are generated from four sets of data collected by disturbing the robot in four major directions, i.e., scenario 1, scenario 2, scenario 3, and scenario 4. The thresholds are extracted separately from each data set and stored according to their respective scenarios. The thresholds which are determined here can also be visualized through matplots. The complete package works on matlab versions 2015 or above.

Folders and contents:
======================
	There are three main folders in this package and their respective usages are briefly explained below.

1. mat_files -	This folder contains 13 matlab files which are used collectively to load the data obtained for each scenario, filter them, analyze them, determine the thresholds for each feature variable and store them as text files. The main file to initiate the aformentioned processes would be 'fall_data_analysis.m'. For more information about each mat file and their respective functions please refer to the matlab files directly, which includes the necessary comments to understand.

2. Simulation Data -	This contains all the data collected by imparting different level of impulses to the robot in four different scenarios (forward, backward, leftside, and rightside). The data collected for four different scenarios are stored in separate folders. The scenarios are numbered depending upon the direction from which the disturbance was given to the robot. The numbering follwed here is as follows:

Scenario_1 - Backward
Scenario_2 - Leftside
Scenario_3 - Forward
Scenario_4 - Rightside

	The folders are named as 'ScenarioX_yyyy', where X is the scenario number and yyyy is the date of data collection. The data file of each experiment are stored in the following format 'falldata_xxx_yy', where xxx is the disturbance magnitude (force in Newton) and yy is the height (in m) at which it was imparted. Apart from the .rtp files, each folder also contains two .txt files, 'scnXF_file.txt' and 'scnXNF_file.txt'. The former file includes the names of non-fall data rtp files and the latter contains the names of those that belong to the fall type.

3. Thresholds -	This is the folder where the thresholds which are determined for each scenario are stored in .txt files. Two text files are generated for each scenario containing the maximum and  minimum values of each feature variables respectively.

4. ros_msg - This folder contains the custom ros message file 'fallState.msg' which is used to collect data from the robot in the desired format. This format has been considered to retrieve and analyze the data from the rtp files using matlab functions.

********************************************************************
Prerequisites:
==============
1. Four sets of fall and non-fall data collected by subjecting a humanoid to different level of disturbances applied at various heights. The height range considered here is 0 - 1.1 m and it is necessary to collect data which spans the whole range. 
2. The collected data should be in the rtp format. 
3. The feature variables and the order in which their respective data needs to be collected are given in the form of a custom rosmsg in the folder ros_msg. Please refer to 'http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv#Creating_a_msg' for creating a custom ros message file. 
4. Follow the format used in the folder 'Simulation Data' for saving the collected data.

*********************************************************************
Things to remember during data collection:
==========================================
1. For data collection different heights should be chosen in such a way that the entire height range is taken into account. For instance, for the data set given in the 'Simulation Data' folder 6 different heights 0.1, 0.3, 0.5, 0.7, 0.9, and 1.1 are considered. 
2. Before giving each disturbance ensure that the complete system has been initialized to capture the unique evolution of feature variables. This helps in making effective comparison between the experiments and among the variables.
3. At each height disturb the robot with the force magnitude increased in an ascending order. For each disturbance, record the feature variables value for atleast 60 secs or until the robot's position stabilizes, whichever is earlier. Data recording should commence even before the application of disturbance to the robot.
4. Save the recorded data set separately depending on the robot's state after the disturbance, i.e., if the robot falls over save it under fall type otherwise store it as a non-fall type data set. 
5. The data published on a rostopic can be recorded in the form of a rosbag and it can be replayed later to save the data in the form of rtp files using the command 'rostopic echo' as follows: 'rostopic echo -p /topic_name > file_path/file_name.rtp'.
6. Generate more non-fall type data sets than fall cases, since it is necessary to accurately determine the safe boundary for a humanoid subjected to varied disturbances. For instance, in the data set given along with this repository, a ratio of 5:1 has been considered between non-fall and fall types. The aforementioned ratio has been maintained for all four scenarios.


